from random import randrange
from random import choice
from random import shuffle

from django.views.generic.base import TemplateView


SELECTION = 15

COLORS = ("orange", "brown", "red", "grey", "pink", "blue")
SIZE = ("small", "tall")
DIRECTION = ("right", "left")

CARAC = {
    "skin": (
        (COLORS[0], "orange_fr.mp3", "Trouve l'écureuil orange."),
        (COLORS[1], "brown_fr.mp3", "Trouve l'écureuil brun."),
        (COLORS[2], "red_fr.mp3", "Trouve l'écureuil rouge."),
        (COLORS[3], "grey_fr.mp3", "Trouve l'écureuil gris."),
    ),
    "height": (
        (SIZE[0], "small_fr.mp3", "Trouve le petit écureuil."),
        (SIZE[1], "tall_fr.mp3", "Hé hé hé, trouve le grand écureuil."),
    ),
    "orientation": (
        (DIRECTION[0], "turn_right_fr.mp3", "Trouve l'écureuil tourné à droite."),
        (DIRECTION[1], "left_fr.mp3", "Hé vas-y, trouve l'écureuil tourné à gauche."),
    ),
    "teeth": (
        (SIZE[0], "small_teeth_fr.mp3", "Trouve l'écureuil avec des petites dents."),
        (SIZE[1], "tall_teeth_fr.mp3", "Trouve l'écureuil avec de grandes dents."),
    ),
    "nose": (
        (COLORS[4], "pink_nose_fr.mp3", "Vas-y, trouve l'écureuil avec le nez rose."),
        (COLORS[5], "blue_nose_fr.mp3", "Trouve l'écureuil avec le nez bleu."),
    ),
}

SQUIRRELS = [
    {"skin": COLORS[0], "height": SIZE[1], "orientation": DIRECTION[0], "teeth": SIZE[1], "nose": COLORS[4]},
    {"skin": COLORS[1], "height": SIZE[1], "orientation": DIRECTION[0], "teeth": SIZE[1], "nose": COLORS[4]},
    {"skin": COLORS[2], "height": SIZE[1], "orientation": DIRECTION[0], "teeth": SIZE[1], "nose": COLORS[4]},
    {"skin": COLORS[3], "height": SIZE[1], "orientation": DIRECTION[0], "teeth": SIZE[1], "nose": COLORS[4]},
    {"skin": COLORS[0], "height": SIZE[1], "orientation": DIRECTION[1], "teeth": SIZE[1], "nose": COLORS[4]},
    {"skin": COLORS[1], "height": SIZE[1], "orientation": DIRECTION[1], "teeth": SIZE[1], "nose": COLORS[4]},
    {"skin": COLORS[2], "height": SIZE[1], "orientation": DIRECTION[1], "teeth": SIZE[1], "nose": COLORS[4]},
    {"skin": COLORS[3], "height": SIZE[1], "orientation": DIRECTION[1], "teeth": SIZE[1], "nose": COLORS[4]},
    {"skin": COLORS[0], "height": SIZE[0], "orientation": DIRECTION[0], "teeth": SIZE[1], "nose": COLORS[4]},
    {"skin": COLORS[1], "height": SIZE[0], "orientation": DIRECTION[0], "teeth": SIZE[1], "nose": COLORS[4]},
    {"skin": COLORS[2], "height": SIZE[0], "orientation": DIRECTION[0], "teeth": SIZE[1], "nose": COLORS[4]},
    {"skin": COLORS[3], "height": SIZE[0], "orientation": DIRECTION[0], "teeth": SIZE[1], "nose": COLORS[4]},
    {"skin": COLORS[0], "height": SIZE[1], "orientation": DIRECTION[0], "teeth": SIZE[1], "nose": COLORS[5]},
    {"skin": COLORS[0], "height": SIZE[1], "orientation": DIRECTION[0], "teeth": SIZE[0], "nose": COLORS[4]},
    {"skin": COLORS[1], "height": SIZE[1], "orientation": DIRECTION[0], "teeth": SIZE[0], "nose": COLORS[4]},
    {"skin": COLORS[2], "height": SIZE[1], "orientation": DIRECTION[0], "teeth": SIZE[0], "nose": COLORS[4]},
    {"skin": COLORS[3], "height": SIZE[1], "orientation": DIRECTION[0], "teeth": SIZE[0], "nose": COLORS[4]},
]

FORREST_NB = 6


class HomeView(TemplateView):
    template_name = "high_ui/splash.html"


class PlayView(TemplateView):
    template_name = "high_ui/play.html"

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context["animals_sprites_size"] = len(SQUIRRELS)*100
        context["forrest_sprites_size"] = FORREST_NB*100
        
        selected_carac_type = choice(list(CARAC))
        selected_carac = randrange(0, len(CARAC[selected_carac_type]), 1)
        context["instruction"] = CARAC[selected_carac_type][selected_carac][2]
        context["instruction_sound"] = CARAC[selected_carac_type][selected_carac][1]

        winner_number = randrange(0, len(SQUIRRELS), 1)
        while SQUIRRELS[winner_number][selected_carac_type] != CARAC[selected_carac_type][selected_carac][0]:
            winner_number = randrange(0, len(SQUIRRELS), 1)

        winner_animal = {
            "winner": True,
            "offset": (len(SQUIRRELS) - winner_number)*100
        }
        context["elements"] = []
        context["elements"].append(winner_animal)

        allowed_squirrels = []

        for i, squirrel in enumerate(SQUIRRELS):
            if squirrel[selected_carac_type] != CARAC[selected_carac_type][selected_carac][0]:
                allowed_squirrels.append(i)

        forrest_elements_nb = randrange(0, SELECTION//2, 1)
        for i in range(1, SELECTION - forrest_elements_nb):
            loser_number = randrange(0, len(allowed_squirrels), 1)

            loser_animal = {
                "winner": False,
                "offset": (len(SQUIRRELS) - allowed_squirrels[loser_number])*100
            }
            context["elements"].append(loser_animal)

        for i in range(0, forrest_elements_nb):
            plant = {
                "winner": None,
                "offset": (FORREST_NB - randrange(0, FORREST_NB, 1))*100
            }
            context["elements"].append(plant)

        shuffle(context["elements"])

        print(context)
        return context
